const path = require('path');
const wce = require('wildcards-entry-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    mode: 'development',
    entry: wce.entry(path.resolve(__dirname, 'client/scripts/**/*.ts')),
    output: {
        path: path.resolve(__dirname, 'build/public/scripts'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    appendTsSuffixTo: [/\.vue$/]
                }
            },
            {
                test: /\.less$/,
                use: [{
                    loader: 'style-loader'
                }, {
                    loader: 'css-loader'
                }, {
                    loader: 'less-loader'
                }]
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{ loader: 'url-loader?limit=10000&mimetype=application/font-woff&name=../fonts/[hash].[ext]' }]
            }, {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: [{ loader: 'url-loader?limit=10000&mimetype=application/octet-stream&name=../fonts/[hash].[ext]' }]
            }, {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                use: [{ loader: 'file-loader?name=../fonts/[hash].[ext]' }]
            }, {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use: [{ loader: 'url-loader?limit=10000&mimetype=image/svg+xml&name=../fonts/[hash].[ext]' }]
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.vue', '.js', '.json', '.less'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    // plugins: [
    //     new ExtractTextPlugin('../styles/main.css')
    // ],
    devtool: 'source-map'
}
