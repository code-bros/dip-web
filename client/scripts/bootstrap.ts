window.dip = {
    bootstrap: async (itemModel, userModel) => {
        const dipClient = await window.System.import('dip-client');
        return dipClient.bootstrap(itemModel, userModel);
    }
};
