import axios from 'axios';
import { MutationTree, ActionTree } from 'vuex';

const mutations: MutationTree<IUser | undefined> = {
    updateAccount: (state, user: IUser) => {
        state.email = user.email;
        state.firstName = user.firstName;
        state.lastName = user.lastName;
    },
    updateUserBio: (state, bio: string) => state.profile.bio = bio,
    addExperience: (state, experience: IExperience) => state.profile.experiences.push(experience),
    removeExperience: (state, exp: IExperience) => {
        const ind = state.profile.experiences.indexOf(exp);
        state.profile.experiences.splice(ind, 1);
    }
};

const actions: ActionTree<IUser | undefined, any> = {
    updateAccount: async ({ commit }, user: IUser) => {
        try {
            await axios.post('/account', user);
            commit('updateAccount', user);
            commit('addMessage', {
                type: 'success',
                text: 'Successfully updated your account'
            });
        } catch (err) {
            commit('addMessage', {
                type: 'danger',
                text: err.message
            });
        }
    },
    updatePassword: async ({ commit }, passwords: string[]) => {
        try {
            await axios.post('/account/password', {
                oldPassword: passwords[0],
                newPassword: passwords[1]
            });
            commit('addMessage', {
                type: 'success',
                text: 'Successfully updated your password'
            });
        } catch (err) {
            commit('addMessage', {
                type: 'danger',
                text: err.message
            });
        }
    },
    updateUserBio: async ({ commit }, bio: string) => {
        try {
            await axios.post('/profile', { bio });
            commit('updateUserBio', bio);
            commit('addMessage', {
                type: 'success',
                text: 'Successfully updated your profile'
            });
        } catch (err) {
            commit('addMessage', {
                type: 'danger',
                text: err.message
            });
        }
    },
    addExperience: async ({ commit }, experience: IExperience) => {
        try {
            await axios.post('/profile/experience', experience);
            commit('addExperience', experience);
            commit('addMessage', {
                type: 'success',
                text: 'Successfully added new experience'
            });
        } catch (err) {
            commit('addMessage', {
                type: 'danger',
                text: err.message
            });
        }
    },
    removeExperience: async ({ commit }, experience: IExperience) => {
        try {
            const { title, subject, type } = experience;

            await axios.delete('/profile/experience', {
                params: { title, subject, type }
            });

            commit('removeExperience', experience);
            commit('addMessage', {
                type: 'success',
                text: 'Successfully removed experience'
            });
        } catch (err) {
            commit('addMessage', {
                type: 'danger',
                text: err.message
            });
        }
    }
};

const state: IUser = window.dip && window.dip.user ? window.dip.user : undefined;

export default {
    state,
    mutations,
    actions
};
