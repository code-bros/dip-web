import { MutationTree, ActionTree } from 'vuex';

const mutations: MutationTree<Message[]> = {
    addMessage: (messages: Message[], newMessage: Message) => {
        newMessage.id = new Date().getTime();
        messages.push(newMessage)
    },
    removeMessage: (messages: Message[], id: number) => {
        const message = messages.find(message => message.id === id);

        if (message) {
            const index = messages.indexOf(message);
            messages.splice(index, 1);
        }
    },
    cleanLastMessage: (messages: Message[]) => messages.pop()
};

const state = [];

export default {
    state,
    mutations
};
