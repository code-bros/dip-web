import Vue from 'vue';
import Vuex from 'vuex';
import { messages, user } from './modules';

// should not load the app config here, it exposes it to the browser
const appConfig: AppConfig = require('../../../config/local.config.json');
import * as moment from 'moment';

Vue.use(Vuex);

Vue.filter('formatDate', (date: Date, format?: string) => {
  if (date) {
    return moment(date).format(format || appConfig.dateFormat)
  }
});

export default new Vuex.Store<AppState>({
  modules: {
    messages,
    user
  }
});
