window.System.config({
    map: {
        'dip-client': '/public/scripts/libs/dip-client-impl.js',
        'react': '/public/scripts/libs/react.production.min.js',
        'react-dom': '/public/scripts/libs/react-dom.production.min.js',
        'vue': '/public/scripts/libs/vue.min.js',
        'vue-class-component': '/public/scripts/libs/vue-class-component.min.js',
        'axios': '/public/scripts/libs/axios.min.js'
    }
});
