import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import { Mutation } from 'vuex-class';
import Messages from './messages.vue';
import store from '../store/store';

@Component({
    store,
    components: { Messages }
})
export default class StoreComponent extends Vue {
    @Mutation('addMessage') commitMessage;

    success(text) {
        this.commitMessage({ type: 'success', text});
    }

    info(text) {
        this.commitMessage({ type: 'info', text});
    }

    warning(text) {
        this.commitMessage({ type: 'warning', text});
    }

    error(text) {
        this.commitMessage({ type: 'danger', text});
    }
}
