declare interface Message {
    id: number;
    type: 'success' | 'info' | 'warning' | 'danger';
    text: string;
}

declare interface IBase {
    id?: string;
    createdOn?: Date;
    modifiedOn?: Date;
}

declare interface IItem extends IBase {
    name: string;
    properties: IProperty[];
    parent?: IContainer;
}

declare interface IWidget extends IItem { }

declare interface IContainer extends IItem {
    children: IItem[];
}

declare interface IDocument extends IContainer { }

declare interface ILayout extends IContainer { }

declare interface IProperty {
    name: string;
    value: string;
    category?: string;
    type?: string;
    description?: string;
}

declare interface IExperience {
    title?: string;
    subject?: string;
    fromDate?: Date;
    toDate?: Date;
    type?: string;
}

declare interface IProfile {
    bio: string;
    renderDocId?: string;
    experiences: IExperience[];
}

declare interface IUser {
    externalId: string;
    name: string;
    email: string;
    firstName: string;
    lastName: string;
    profile?: IProfile;
}

declare interface AppState {
    messages: Message[],
    user?: IUser
}
