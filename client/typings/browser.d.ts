import { System } from 'systemjs';

declare global {
    interface Window {
        System: System,
        dip: {
            bootstrap: Function,
            user?: IUser
        }
    }
}
