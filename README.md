# DIP Web

1. Create local mongo container ```docker run --name mongo -p 27017:27017 d mongo```
2. Create local redis container for the web sessions ```docker run --name redis -p 6379:6739 d redis```
3. Start the server dev watch ```npm run dev:server```
4. Start the static dev watch ```npm run dev:client```
5. Start the dip-cv-rest app on port 3000
6. Access this [http://localhost:8080](http://localhost:8080) 