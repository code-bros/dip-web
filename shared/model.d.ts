declare interface AccountForm {
  email: string;
  firstName: string;
  lastName: string;
}
