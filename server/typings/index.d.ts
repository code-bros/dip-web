declare interface AppConfig {
    serviceEndpoint: string;
    dbHost: string;
    sessionSecret: string;
    dateFormat: string;
}

/** Allows loading for json files with import **/
declare module '*.json' {
    const value: any;
    export default value;
}

declare namespace Express {
    export interface Request {
        session: {
            user?: any,
            token?: string,
            destroy: Function
        }
    }
}
