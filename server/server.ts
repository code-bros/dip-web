import { resolve, join } from 'path';
import { static as staticMiddleware } from 'express';
import { json, urlencoded } from 'body-parser';
import { ServerLoader, ServerSettings, Inject } from 'ts-express-decorators';
import { AuthHeaderMiddleware, LocalsMiddleware, SessionMiddleware } from './middlewares';
import { LoginService } from './services';
import compress = require('compression');
import cookieParser = require('cookie-parser');
import cors = require('cors');

@ServerSettings({
    rootDir: resolve(__dirname),
    port: 8080,
    acceptMimes: ['application/json'],
    componentsScan: [
        '${rootDir}/middlewares/**/*.ts',
        '${rootDir}/services/**/**.ts'
    ],
    mount: {
        '/': '${rootDir}/controllers/**/*.ts'
    }
})
export class Server extends ServerLoader {
    @Inject()
    $onMountingMiddlewares(loginService: LoginService) {
        this.use(cors({
            origin: '/'
        }));

        this.set('view engine', 'pug');
        this.set('views', join(__dirname, '/views'));
        this.use('/public', staticMiddleware(join(__dirname, '/public')));
        this.use(cookieParser());
        this.use(SessionMiddleware);
        this.use(loginService.init())
        this.use(loginService.session());
        this.use(LocalsMiddleware);
        this.use(AuthHeaderMiddleware);
        this.use(json());
        this.use(urlencoded({ extended: true }));
        this.use(compress());
    }

    $onReady() {
        const settings = this.getSettingsService();
        console.log(`Server started on port ${settings.port}`);
    }

    $onServerInitError(err) {
        console.error(err);
    }
}

new Server().start();
