import { Service } from 'ts-express-decorators';
import { Item, Container, Template, View, RootView } from '../models';
import { TemplateService } from '.';

interface TemplateMap {
    [name: string]: Template | boolean;
}

const templateMap: TemplateMap = {};

function bfs(root: Item, handler: (current: Item) => void) {
    const queue: Item[] = [root];

    while (queue.length) {
        const current: Item | undefined = queue.pop();

        if (current) {
            handler(current);

            if (current instanceof Container) {
                queue.unshift(...current.getChildren());
            }
        }
    }
}

@Service()
export default class ViewAssemblerService {
    constructor(private templateService: TemplateService) { }

    async assemble(item: Item): Promise<any> {
        const templateName = item.getProperty('template');

        if (templateName) {
            const templateMap = await this.loadTemplates(item);

            const rootView = new RootView(item, <Template>templateMap[templateName.value]);

            if (item instanceof Container) {
                const childViews = rootView.getChildren();

                item.getChildren().forEach(childItem => {
                    const childView = this.buildView(childItem, rootView, templateMap);

                    if (childView) {
                        childViews.push(childView);
                    }
                });
            }

            return rootView;
        }

        throw new Error('No template associated with item');

    }

    private async loadTemplates(root: Item): Promise<TemplateMap> {
        const templateNames: string[] = [];

        bfs(root, item => {
            const templateProp = item.getProperty('template');
            const templateName = templateProp && templateProp.value;

            if (templateName && templateNames.indexOf(templateName) === -1) {
                templateNames.push(templateName);
            }
        });

        const templates = await this.templateService.find({ name: templateNames });

        return templates.reduce((map, current) => {
            map[current.getName()] = current;
            return map;
        }, templateMap);
    }

    private buildView(item: Item, rootView: RootView, templateMap: TemplateMap): View | undefined {
        const templateProp = item.getProperty('template');

        if (templateProp) {
            const template = <Template>templateMap[templateProp.getValue()];

            if (template) {
                rootView.getStyles().push(...template.getAssets('styles'));
                rootView.getScripts().push(...template.getAssets('scripts'));
                const view = new View(item, template);

                if (item instanceof Container) {
                    const childViews = view.getChildren();

                    item.getChildren().forEach(childItem => {
                        const childView = this.buildView(childItem, rootView, templateMap);

                        if (childView) {
                            childViews.push(childView);
                        }
                    });
                }

                return view;
            }
        }
    }
}