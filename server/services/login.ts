import { Service } from 'ts-express-decorators';
import * as passport from 'passport';
import { Strategy } from 'passport-local';
import { UserService, TokenService } from '.';
import User from '../models/user';
import { Request, Response } from 'express';

@Service()
export default class LoginService {
    constructor(private userService: UserService, private tokenService: TokenService) {
        passport.use('login', new Strategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        }, this.onLocalLogin));

        passport.serializeUser(this.serialize);
        passport.deserializeUser(this.deserialize);
    }

    init() {
        return passport.initialize();
    }

    session(options?) {
        return passport.session(options);
    }

    login(req: Request, res: Response): Promise<User> {
        return new Promise<User>((res, rej) => {
            passport.authenticate('login', (err, user: User) => {
                if (!err) {
                    res(this.doLogin(req, user));
                } else {
                    rej(err);
                }
            })(req, res, () => { });
        });
    }

    autoLogin(req: Request, user: User): Promise<User> {
        return this.doLogin(req, user);
    }

    logout(req: Request) {
        req.logout();
    }

    private doLogin(req: Request, user: User): Promise<User> {
        return new Promise<User>((res, rej) => {
            req.login(user, async err => {
                if (!err) {
                    const token = await this.tokenService.getUserToken(user.getExternalId());
                    req.session.token = token;
                    res(user);
                } else {
                    rej(err);
                }
            });
        });
    }

    private onLocalLogin = async (req, username, password, done) => {
        const user = await this.userService.getWithCredentials(username, password);
        done(null, user || false);
    };

    private serialize = (user: User, done) => {
        done(null, user.getId());
    }

    private deserialize = async (id: string, done) => {
        const user = await this.userService.findById(id, true);
        done(null, user);
    }
}
