import { Service } from 'ts-express-decorators';
import { ICollection } from 'monk';
import Store from '../store';
import { Profile, Experience } from '../models';

@Service()
export default class ProfileService {
  protected collection: ICollection;

  constructor(store: Store) {
    this.collection = store.get('profiles');
  }

  async create(userId: string, profileModel: any): Promise<Profile> {
    const profileModelAfterInsert = await this.collection.insert(Object.assign({ userId }, profileModel));
    return new Profile(profileModelAfterInsert);
  }

  async get(userId: string): Promise<Profile> {
    const profileModel = await this.collection.findOne({ userId });

    if (profileModel) {
      return new Profile(profileModel);
    }
  }

  async addExperience(userId: string, experienceModel: any): Promise<Profile> {
    const profileModel = await this.collection.findOne({ userId });

    if (profileModel) {
      profileModel.experiences.push(experienceModel);
      const updatedProfile = await this.collection.findOneAndUpdate({ userId }, profileModel);
      return new Profile(updatedProfile);
    }
  }

  async removeExperience(userId: string, query: { title: string, subject: string, type: string }): Promise<Profile> {
    const profileModel = await this.collection.findOne({ userId });

    if (profileModel) {
      // find indexes to remove
      const indexesToRemove = profileModel.experiences.filter(exp => exp.title === query.title &&
        exp.subject === query.subject &&
        exp.type === query.type).reduce((indexArr, exp) => {
          const ind = profileModel.experiences.indexOf(exp);

          if (ind !== -1) {
            indexArr.push(ind);
          }

          return indexArr;
        }, []);

      // remove all items with the found indexes
      indexesToRemove.forEach(ind => profileModel.experiences.splice(ind, 1));

      // persist changes
      const updatedProfile = await this.collection.findOneAndUpdate({ userId }, profileModel);
      return new Profile(updatedProfile);
    }
  }

  async update(userId: string, updatedModel: any): Promise<Profile> {
    const profileModel = await this.collection.findOne({ userId });

    if (profileModel) {
      const updatedProfile = await this.collection.findOneAndUpdate({ userId }, Object.assign(profileModel, updatedModel));
      return new Profile(updatedProfile);
    }
  }
}
