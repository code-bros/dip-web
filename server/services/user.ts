import { Service } from 'ts-express-decorators';
import { BaseService, ProfileService } from '.';
import { User } from '../models';
import { ICollection } from 'monk';
import Store from '../store';
import { id } from 'monk';

@Service()
export default class UserService extends BaseService<User> {
    protected collection: ICollection;

    constructor(store: Store, private profileService: ProfileService) {
        super('users', data => new User(data));
        this.collection = store.get('users');
    }

    async register(model: any): Promise<User> {
        const { name, firstName, lastName, email, password } = model;
        const externalUser = await this.create({ name }) as any;

        const { id, type, createdOn, modifiedOn } = externalUser;

        const userModel = await this.collection.insert({
            externalId: id,
            name,
            firstName,
            lastName,
            email,
            password,
            createdOn,
            modifiedOn
        });

        return this.mapper(userModel);
    }

    async findById(idStr: string, loadProfile = false): Promise<User> {
        const userData = await this.collection.findOne({ _id: id(idStr) });

        if (userData) {
            const user = this.mapper(userData);

            if (loadProfile) {
                const profile = await this.profileService.get(user.getId());

                if (profile) {
                    user.setProfile(profile);
                }
            }

            return user;
        }

        throw new Error('User could not be found');
    }


    async getWithCredentials(name: string, password: string): Promise<User> {
        const userData = await this.collection.findOne({ name, password });

        if (userData) {
            return this.mapper(userData);
        }

        throw new Error('Credentials not correct');
    }

    async update(userId: string, model): Promise<User> {
        const userModel = await this.collection.findOne({ _id: id(userId) });

        if (userModel) {
            const updatedUserModel = await this.collection.findOneAndUpdate({ _id: id(userId) }, Object.assign(userModel, model));
            return this.mapper(updatedUserModel);
        }
    }
}
