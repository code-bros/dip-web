import { Service } from 'ts-express-decorators';
import { BaseService } from '.';
import { Template } from '../models';

@Service()
export default class TemplateService extends BaseService<Template> {
    constructor() {
        super('templates', data => new Template(data));
    }
}
