import BaseService from './base';
import DocumentService from './document';
import TemplateService from './template';
import ProfileService from './profile';
import UserService from './user';
import TokenService from './token';
import ViewAssemblerService from './view-assembler';
import LoginService from './login';

export {
    BaseService,
    DocumentService,
    TemplateService,
    ProfileService,
    UserService,
    TokenService,
    ViewAssemblerService,
    LoginService
};
