import { Service } from 'ts-express-decorators';
import axios from '../http/axios';
const appConfig: AppConfig = require('../../config/local.config.json');

@Service()
export default class TokenService {
    protected endpoint: string;

    constructor() {
        this.endpoint = `${appConfig.serviceEndpoint}/tokens/issue`;
    }

    async getUserToken(id: string) {
        const res = await axios.get(`${this.endpoint}/${id}`);
        return res.data.token;
    }
}
