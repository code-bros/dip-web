import { Service } from 'ts-express-decorators';
import { BaseService } from '.';
import { Item, Document } from '../models';

@Service()
export default class DocumentService extends BaseService<Document> {
    constructor() {
        super('documents', Item.map);
    }
}
