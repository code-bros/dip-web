import { Base } from '../models';
import axios from '../http/axios';
const appConfig: AppConfig = require('../../config/local.config.json');

export default abstract class BaseService<T extends Base> {
    protected endpoint: string;
    protected mapper: (data: any) => T;

    constructor(resourceEndpoint: string, mapper: (data: any) => T) {
        this.endpoint = `${appConfig.serviceEndpoint}/${resourceEndpoint}`;
        this.mapper = mapper;
    }

    async create(model: any): Promise<T> {
        const res = await axios.post(`${this.endpoint}`, model);
        return this.mapper(res.data);
    }

    async findOne(params?: any): Promise<T> {
        const res = await axios.get(`${this.endpoint}/single`, { params });
        return this.mapper(res.data);
    }

    async find(params?: any): Promise<T[]> {
        const res = await axios.get(`${this.endpoint}/`, { params });
        return res.data.map(dataItem => this.mapper(dataItem));
    }
}
