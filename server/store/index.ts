import monk, { ICollection } from 'monk';
import { Service } from 'ts-express-decorators';
const appConfig: AppConfig = require('../../config/local.config.json');

const db = monk(appConfig.dbHost);

db.catch(err => {
    console.log('Could not initialize the db');
    process.exit(1);
});

process.on('SIGTERM', function () {
    db.close();
});

@Service()
export default class Store {
    get(name: string): ICollection {
        return db.get(name);
    }
}
