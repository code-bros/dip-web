import Base from './base';
import Property from './property';

export default class Template extends Base {
    protected name: string;
    protected view: string;
    protected properties: Property[];

    constructor(model: any) {
        super(model);
        this.name = model.name;
        this.view = model.view;
        this.properties = model.properties.map(propModel => new Property(propModel));
    }

    getName(): string {
        return this.name;
    }

    getView(): string {
        return this.view;
    }

    getProperties(): Property[] {
        return this.properties;
    }

    getProperty(name: string): Property | undefined {
        return this.properties.find(prop => prop.getName() === name);
    }

    getAssets(category: string): Property[] {
        return this.properties.filter(prop => prop.getType() === 'asset' && prop.getCategory() === category);
    }

    getMain(): Property | undefined {
        return this.properties.find(prop => prop.getName() === 'main' && prop.getCategory() === 'scripts');
    }
}
