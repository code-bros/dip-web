import Item from './item';
import Widget from './widget';
import Document from './document';
import Layout from './layout';
import Property from './property';
import Template from './template';

function getType(item: Item): string | undefined {
    if (item instanceof Document) {
        return 'document';
    } else if (item instanceof Layout) {
        return 'layout';
    } else if (item instanceof Widget) {
        return 'widget';
    }
}

export type ViewProperties = {
    [name: string]: string;
}

export default class View {
    protected id?: string;
    protected name: string;
    protected properties: ViewProperties;
    protected item: Item;
    protected template: Template;
    protected children: Array<View>;

    constructor(item: Item, template: Template) {
        this.id = item.getId();
        this.name = item.getName();

        this.properties = item.getProperties().reduce((props: any, prop: Property) => {
            props[prop.name] = prop.value;
            return props;
        }, {});

        this.item = item;
        this.template = template;
        this.children = [];
    }

    getId(): string | undefined {
        return this.id;
    }

    getName(): string {
        return this.name;
    }

    getProperties(): ViewProperties {
        return this.properties;
    }

    getTemplate(): Template {
        return this.template;
    }

    getChildren(): Array<View> {
        return this.children;
    }

    getClientModel(): any {
        const mainScriptProp: Property | undefined = this.template.getMain();

        const model: any = Object.assign({}, this.item, { type: getType(this.item) });

        model.children = model.children || [];

        if (mainScriptProp) {
            model.main = mainScriptProp.getValue();
        }

        this.children.reduce((prev: Array<any>, curr: View) => {
            prev.push(curr.getClientModel());
            return prev;
        }, model.children);

        return model;
    }
}

export class RootView extends View {
    protected styles: Property[];
    protected scripts: Property[];

    constructor(item: Item, template: Template) {
        super(item, template);
        this.styles = template.getAssets('styles');
        this.scripts = template.getAssets('scripts');
    }

    getStyles(): Property[] {
        return this.styles;
    }

    getScripts(): Property[] {
        return this.scripts;
    }
}
