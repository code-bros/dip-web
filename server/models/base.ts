export default abstract class Base {
    protected id?: string;
    protected createdOn?: Date;
    protected modifiedOn?: Date;

    constructor(model: any) {
        const modelId = model._id || model.id;

        if (modelId) {
            this.id = modelId.toString();

            if (modelId.getTimestamp) {
                this.createdOn = modelId.getTimestamp();
            }

            this.modifiedOn = model.modifiedOn;
        }
    }

    getId(): string | undefined {
        return this.id;
    }

    getCreatedOn(): Date | undefined {
        return this.createdOn;
    }

    getModifiedOn(): Date | undefined {
        return this.modifiedOn;
    }
}
