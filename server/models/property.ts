export default class Property {
    name: string;
    value: string;
    category?: string;
    type?: string;
    description?: string;

    constructor(model: any) {
        this.name = model.name;
        this.value = model.value;
        this.category = model.category;
        this.type = model.type;
        this.description = model.description;
    }

    getName(): string {
        return this.name;
    }

    getValue(): string {
        return this.value;
    }

    getCategory(): string | undefined {
        return this.category;
    }

    getType(): string | undefined {
        return this.type;
    }

    getDescription(): string | undefined {
        return this.description;
    }
}
