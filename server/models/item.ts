import { Base, Container, Document, Layout, Widget, Property } from '.';

export default class Item extends Base {
    protected name: string;
    protected properties: Property[];
    protected parent?: Container;

    constructor(model: any) {
        super(model);
        this.name = model.name;
        this.properties = model.properties.map(propModel => new Property(propModel));
    }

    getName(): string {
        return this.name;
    }

    getProperties(): Property[] {
        return this.properties;
    }

    getProperty(name: string): Property | undefined {
        return this.properties.find(prop => prop.getName() === name);
    }

    static map<T extends Item>(model: any): T {
        switch (model.type) {
            case 'document':
                return new Document(model) as any;
            case 'layout':
                return new Layout(model) as any;
            case 'widget':
                return new Widget(model) as any;
        }
    }

    getParent(): Container | undefined {
        return this.parent;
    }
}
