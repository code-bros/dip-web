import Base from './base';

export class Experience {
    protected id: string;
    protected title: string;
    protected subject: string;
    protected fromDate: Date;
    protected toDate?: Date;
    protected type: string;

    constructor(model: any) {
        this.title = model.title;
        this.subject = model.subject;
        this.fromDate = new Date(model.fromDate);

        if (model.toDate) {
            this.toDate = new Date(model.toDate);
        }

        this.type = model.type;
    }

    getTitle(): string {
        return this.title;
    }

    getSubject(): string {
        return this.subject;
    }

    getFromDate(): Date {
        return this.fromDate;
    }

    getToDate(): Date | undefined {
        return this.toDate;
    }

    getType(): string {
        return this.type;
    }
}

export class Profile {
    protected bio: string;
    protected renderDocId?: string;
    protected experiences: Experience[];

    constructor(model: any) {
        this.bio = model.bio;
        this.renderDocId = model.renderDocId;
        this.experiences = model.experiences.map((expModel: any) => new Experience(expModel));
    }

    getBio(): string {
        return this.bio;
    }

    getRenderDocId(): string | undefined {
        return this.renderDocId;
    }

    getExperiences(): Experience[] {
        return this.experiences;
    }
}

export default class User extends Base {
    protected externalId: string;
    protected name: string;
    protected email: string;
    protected firstName: string;
    protected lastName: string;
    protected profile?: Profile;

    constructor(model: any) {
        super(model);
        this.externalId = model.externalId;
        this.name = model.name;
        this.email = model.email;
        this.firstName = model.firstName;
        this.lastName = model.lastName;

        if (model.profile) {
            this.profile = new Profile(model.profile);
        }
    }

    getExternalId(): string {
        return this.externalId;
    }

    getName(): string {
        return this.name;
    }

    getEmail(): string {
        return this.email;
    }

    getFirstName(): string {
        return this.firstName;
    }

    getLastName(): string {
        return this.lastName;
    }

    getProfile(): Profile {
        return this.profile;
    }

    setProfile(profile: Profile) {
        this.profile = profile;
    }
}
