import { Item } from '.';

export default abstract class Container extends Item {
    protected children: Item[];

    constructor(model: any) {
        super(model);
        this.children = [];
        this.mapChildren(model.children);
    }

    getChildren(): Item[] {
        return this.children;
    }

    private mapChildren(children: any[]) {
        children.forEach(childModel => {
            const child = Item.map<Item>(childModel);

            if (child) {
                this.children.push(child);
            }
        });
    }
}
