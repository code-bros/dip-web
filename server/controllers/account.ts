import { Request, Response } from 'express';
import { Controller, Get, Render, Post, Redirect, BodyParams, Req, Res, Authenticated } from 'ts-express-decorators';
import { LoginService, UserService } from '../services';

@Controller('/account')
export default class AccountCtrl {
    constructor(private loginService: LoginService, private userService: UserService) { }

    @Get('/')
    @Authenticated()
    @Render('account', { pageName: 'Account Settings', title: 'Account Settings' })
    getAccount(@Req() req: Request) { }

    @Get('/login')
    @Render('login', { pageName: 'Login' })
    getLogin() { }

    @Get('/logout')
    @Authenticated()
    @Redirect('/')
    getLogout(@Req() req: Request) {
        return this.loginService.logout(req);
    }

    @Post('/')
    @Authenticated()
    async postAccount(@Req() req: Request, @BodyParams() model: AccountForm) {
        return await this.userService.update(req.user.getId(), model);
    }

    @Post('/password')
    @Authenticated()
    async postAccountPassword(@Req() req: Request, @BodyParams() model) {
        const user = req.user;
        await this.userService.getWithCredentials(user.getName(), model.oldPassword);
        return await this.userService.update(user.getId(), {
            password: model.newPassword
        });
    }

    @Post('/login')
    @Redirect('/')
    postLogin(@Req() req: Request, @Res() res: Response) {
        return this.loginService.login(req, res);
    }
}
