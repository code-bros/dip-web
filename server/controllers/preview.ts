import { Controller, Get, Render, PathParams, Authenticated } from 'ts-express-decorators';
import { DocumentService, ViewAssemblerService } from '../services';

@Authenticated()
@Controller('/preview')
export default class PreviewCtrl {
    constructor(private documentService: DocumentService, private viewAssemblerService: ViewAssemblerService) { }

    @Get('/:name')
    @Render('preview')
    async byName(@PathParams('name') name: string) {
        const doc = await this.documentService.findOne({ name });
        const view = await this.viewAssemblerService.assemble(doc);
        return { view };
    }
}
