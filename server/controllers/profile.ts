import { Controller, Authenticated, Get, Post, BodyParams, Render, Req, Delete, QueryParams } from 'ts-express-decorators';
import { Request } from 'express';
import { ProfileService } from '../services';

@Authenticated()
@Controller('/profile')
export default class ProfileCtrl {
    constructor(private profileService: ProfileService) { }

    @Get('/')
    @Authenticated()
    @Render('profile', { pageName: 'Profile', title: 'Your Profile' })
    getProfile(@Req() req: Request) {
        return { profile: req.user.getProfile() };
    }

    @Post('/')
    @Authenticated()
    postProfile(@Req() req: Request, @BodyParams() model) {
        return this.profileService.update(req.user.getId(), model);
    }

    @Post('/experience')
    @Authenticated()
    postExperience(@Req() req: Request, @BodyParams() model) {
        return this.profileService.addExperience(req.user.getId(), model);
    }

    @Delete('/experience')
    @Authenticated()
    async deleteExperience(@Req() req: Request, @QueryParams() query) {
        return this.profileService.removeExperience(req.user.getId(), query);
    }
}
