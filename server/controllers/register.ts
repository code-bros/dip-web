import { Controller, Get, Render, Post, BodyParams, Redirect, Authenticated, Req, PathParams } from 'ts-express-decorators';
import { Request } from 'express';
import { UserService, LoginService, ProfileService, DocumentService } from '../services';
import { User } from '../models';

@Controller('/register')
export default class RegisterCtrl {
    constructor(private userService: UserService,
        private profileService: ProfileService,
        private loginService: LoginService,
        private documentService: DocumentService) { }

    @Get('/')
    @Render('register/account', { pageName: 'Register Account', title: 'Step 1. Account Details' })
    getAccount() { }

    @Get('/profile')
    @Authenticated()
    @Render('register/profile', { pageName: 'Create Profile', title: 'Step 2. Profile Details' })
    getProfile() { }

    @Get('/finish')
    @Authenticated()
    @Render('register/finish', { pageName: 'Select Template', title: 'Step 3. Select Template' })
    async getFinish() {
        const documents = await this.documentService.find();
        return { documents };
    }

    @Post('/')
    @Redirect('/register/profile')
    async postRegister(@Req() req: Request, @BodyParams() userModel) {
        const user = await this.userService.register(userModel);
        return this.loginService.autoLogin(req, user);
    }

    @Post('/profile')
    @Authenticated()
    @Redirect('/register/finish')
    postProfile(@Req() req: Request, @BodyParams() profileModel) {
        const userId = req.user.getId();
        return this.profileService.create(userId, profileModel);
    }

    @Post('/finish')
    @Authenticated()
    @Redirect('/')
    postFinish(@Req() req: Request, @BodyParams() profileUpdates) {
        const userId = req.user.getId();
        return this.profileService.update(userId, profileUpdates);
    }
}
