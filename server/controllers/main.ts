import { Request, Response } from 'express';
import { Controller, Get, Req, Render, Authenticated } from 'ts-express-decorators';
import { DocumentService } from '../services';

@Controller('')
export default class MainCtrl {
    constructor(private documentService: DocumentService) { }

    @Get('')
    @Render('index', { pageName: 'Welcome!' })
    getIndex(req: Request, res: Response) {
        if (req.isAuthenticated()) {
            res.redirect('/home');
        }
    }

    @Get('/home')
    @Authenticated()
    @Render('home', { pageName: 'Welcome Home!' })
    async getHome(@Req() req: Request) {
        const id = req.user.getProfile().getRenderDocId();
        const document = await this.documentService.findOne({ id });
        return { document };
    }
}
