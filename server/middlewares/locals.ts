import { Request, Response } from 'express';
import { Middleware, IMiddleware } from 'ts-express-decorators';
import { View, RootView } from '../models';
const appConfig: AppConfig = require('../../config/local.config.json');
import * as moment from 'moment';
import * as handlebars from 'handlebars';

function render(view: View) {
    return new handlebars.SafeString(handlebars.compile(view.getTemplate().getView())(view));
}

function eq(valueOne: any, valueTwo: any) {
    return valueOne == valueTwo;
}

handlebars.helpers.render = render;
handlebars.helpers.eq = eq;

@Middleware()
export default class LocalsMiddleware implements IMiddleware {
    use(req: Request, res: Response) {
        res.locals.user = req.user;

        res.locals.formatDate = (date: Date) => {
            return moment(date).format(appConfig.dateFormat);
        };

        res.locals.json = obj => {
            return JSON.stringify(obj);
        };

        res.locals.render = render;
    }
}
