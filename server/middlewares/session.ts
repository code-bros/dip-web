import { Request, Response, NextFunction } from 'express';
import { Middleware, IMiddleware } from 'ts-express-decorators';
import session = require('express-session');
const RedisStore = require('connect-redis')(session);

const sessionSettings = {
    name: 'sid',
    secret: 'my-secret',
    store: new RedisStore(),
    cookie: {
        maxAge: 1850000
    },
    resave: true,
    saveUninitialized: true
};

@Middleware()
export default class SessionMiddleware implements IMiddleware {
    use(req: Request, res: Response, next: NextFunction) {
        return session(sessionSettings)(req, res, next);
    }
}
