import { Request, Response, NextFunction } from 'express';
import { AxiosRequestConfig } from 'axios';
import { Middleware, Req, Res, Next, IMiddleware } from 'ts-express-decorators';
import axios from '../http/axios';
import { $log } from 'ts-log-debug';

@Middleware()
export default class AuthHeaderMiddleware implements IMiddleware {
    private token: string;
    private isConfigured = false;

    use(@Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        // closure helps pass the token in the interceptor of axios
        this.token = req.session.token;

        // prevent the interceptor to be attached multiple times
        if (!this.isConfigured) {
            this.isConfigured = true;

            axios.interceptors.request.use((config: AxiosRequestConfig) => {
                if (this.token) {
                    $log.debug(`axios will use token ${this.token}`);
                    config.headers['Authorization'] = this.token;
                }

                return config;
            });
        }

        next();
    }
}
