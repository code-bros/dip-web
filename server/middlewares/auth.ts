import { Request, Response, NextFunction } from 'express';
import { OverrideMiddleware, IMiddleware } from 'ts-express-decorators';
import { AuthenticatedMiddleware } from 'ts-express-decorators/lib/mvc/components/AuthenticatedMiddleware';

@OverrideMiddleware(AuthenticatedMiddleware)
export default class AuthMiddleware implements IMiddleware {
    use(req: Request, res: Response, next: NextFunction) {
        if (!req.isAuthenticated()) {
            res.redirect('/');
        } else {
            next();
        }
    }
}
