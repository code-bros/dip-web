const gulp = require('gulp');
const path = require('path');
const ts = require('gulp-typescript');
const nodemon = require('gulp-nodemon');
const tsProject = ts.createProject('tsconfig.json');
const less = require('gulp-less');

gulp.task('less', ['fonts'], function () {
    return gulp.src('./client/styles/main.less')
        .pipe(less({
            paths: [
                'node_modules'
            ]
        }))
        .pipe(gulp.dest(path.join(__dirname, 'build/public/styles')));
});

gulp.task('fonts', function () {
    gulp.src('./node_modules/bootstrap-less/fonts/*')
        .pipe(gulp.dest('./build/public/fonts'));
});

gulp.task('server', function () {
    nodemon({
        watch: ['./build/'],
        script: './build/server.js',
        ignore: ['./build/public']
    });
})

gulp.task('copy:libs', function () {
    gulp.src([
        './node_modules/dip-client-impl/dist/dip-client-impl.js',
        './node_modules/react/umd/react.production.min.js',
        './node_modules/react-dom/umd/react-dom.production.min.js',
        './node_modules/systemjs/dist/system.js',
        './node_modules/vue/dist/vue.min.js',
        './node_modules/vue-class-component/dist/vue-class-component.min.js',
        './node_modules/axios/dist/axios.min.js'
    ]).pipe(gulp.dest('./build/public/scripts/libs'));
});

gulp.task('copy:views', function () {
    gulp.src('./server/views/**/*')
        .pipe(gulp.dest('./build/views'));
});

gulp.task('copy', ['copy:libs', 'copy:views']);

gulp.task('tsc', function () {
    var tsResult = gulp.src('./server/**/*.ts').pipe(tsProject());
    return tsResult.js.pipe(gulp.dest('./build'));
});

gulp.task('build', ['tsc', 'less', 'copy']);

gulp.task('watch', ['tsc', 'copy', 'less', 'server'], function () {
    gulp.watch('./server/**/*.ts', ['tsc']);
    gulp.watch('./server/views/**/*.pug', ['copy:views']);
    gulp.watch('./client/**/*.less', ['less']);
});
